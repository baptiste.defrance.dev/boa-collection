#!/usr/bin/env python3
"""
Tool to get information from themoviedb.org
very basic
"""

import sys
from os import listdir
from os.path import isfile, join
from themoviedb import TMDb
from datetime import datetime

MD_FORMAT = """\
---
tags: movie
LastSeen: {}
Rewatch: {}
previous-watches: {}
Title: {}
Released: {}
Categories: {}
Director: {}
Cast: {}
Duration: {}
language: {}
imdb-link: {}
added: {}
collection: {}
---
![{}]({})
{}
#### Overview:
{}

#### Notes:
{}
"""

HELP_STR = "Creates a markdown file with frontmatter metadata from themoviedb.org"

# SD
POSTER_URL_FMT = "https://www.themoviedb.org/t/p/w185{}"

def get_movie(search_str, released=None):
    """Gets data from themoviedb.org"""
    tmdb = TMDb(key="c47a68e6663fb95a303c734ceaa84f9a", language="en-US", region="FR")
    movies = tmdb.search().movies(search_str)

    movie_id = None

    if released:
        for mov in movies:
            details = tmdb.movie(mov.id).details()
            if str(details.year) == str(released):
                movie_id = mov.id
                break
    else:
        try:
            movie_id = movies[0].id  # get first result
        except IndexError:
            sys.stderr.write("No results for this search\n")
            return None

    if movie_id:
        return  tmdb.movie(movie_id).details(append_to_response="credits,external_ids")

    sys.stderr.write("No results for this search\n")
    return None

def get_data(movie_fname):
    """Extracts data from a markdown file"""
    data = {"Rewatch": '', "previous-watches": '',
            "Title": '', "LastSeen": '',
            "notes": '', "Released": '', "collection": ''}
    with open(movie_fname, 'r', encoding="utf-8") as movie_file:
        notes = False
        notescontent = ""
        for lnumber, line in enumerate(movie_file):
            lcontent = line.strip()
            if not lnumber or (lnumber < 2 and lcontent == "---"):
                continue

            if lcontent == "---":
                notes = True
                continue

            if notes:
                notescontent += lcontent + '\n'
            elif lcontent.startswith("Rewatch:"):
                data["Rewatch"] = lcontent.split("Rewatch: ")[1]
            elif lcontent.startswith("Title:"):
                data["Title"] = lcontent.split("Title: ")[1]
            elif lcontent.startswith("Released:"):
                data["Released"] = lcontent.split("Released: ")[1]
            elif lcontent.startswith("collection:"):
                data["collection"] = lcontent.split("collection: ")[1]
            elif (lcontent.startswith("LastSeen:") and
                  len(lcontent.split("LastSeen: ")) == 2):
                data["LastSeen"] = lcontent.split("LastSeen: ")[1]
            elif (lcontent.startswith("previous-watches:") and
                  len(lcontent.split("previous-watches: ")) == 2):
                data["previous-watches"] = lcontent.split("previous-watches: ")[1]

        data["notes"] = notescontent
    return data

def get_director(crew):
    """Gets the director from the crew"""
    for person in crew:
        if person.job == "Director":
            return person.name
    return ""

def get_cast(cast, count=6):
    """Gets the first `count` people in cast (default=6)"""
    people = []
    for person in cast[:count:]:
        people.append(person.name)

    return ", ".join(people)

def format_movie(data, tmdb_movie):
    """
    data: {"Rewatch": '',
           "previous-watches": '',
           "Title": '',
           "LastSeen": '',
           "notes": '',
           "Released": ''}
    """
    categories = ", ".join(g.name for g in tmdb_movie.genres)
    director = get_director(tmdb_movie.credits.crew)
    cast = get_cast(tmdb_movie.credits.cast)
    duration_h, duration_m = divmod(tmdb_movie.runtime, 60)
    poster_tag = "poster"
    poster_url = POSTER_URL_FMT.format(tmdb_movie.poster_path)
    notes = data["notes"]
    today = "{:%Y-%m-%d}".format(datetime.now())

    return MD_FORMAT.format(data["LastSeen"],
                            data["Rewatch"],
                            data["previous-watches"],
                            tmdb_movie.title.replace(':', " -"),
                            tmdb_movie.year,
                            categories,
                            director,
                            cast,
                            f"{duration_h}h{duration_m}",
                            tmdb_movie.original_language,
                            tmdb_movie.external_ids.imdb_url,
                            today,
                            data["collection"],
                            poster_tag,
                            poster_url,
                            tmdb_movie.tagline,
                            tmdb_movie.overview,
                            notes)

def write_movie(filename, text):
    """writes the movie data to the file given as argument"""
    if filename == "-":
        sys.stdout.write(text)
        return

    with open(filename, 'w', encoding="utf-8") as outfile:
        outfile.write(text)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=HELP_STR)
    parser.add_argument('-f', "--folder", type=str, help="Add a folder input")
    parser.add_argument('-m', "--manual", type=str, help="Manual search")
    parser.add_argument('-y', "--year", type=str, help="Adds a year as a search criteria")
    parser.add_argument('-c', "--collection", type=str, help="Add to collection")
    parser.add_argument('OUTPUT', nargs='?', type=str, help="Output folder for the markdown files")
    args = parser.parse_args()

    if not args.folder and not args.manual:
        sys.stderr.write("No input specified.")
        sys.exit(1)

    if not args.OUTPUT:
        args.OUTPUT = "-"

    if args.folder:
        filelist = [f for f in listdir(args.folder) if isfile(join(args.folder, f))]
        for fname in filelist:
            movie_data = get_data(args.folder + '/' + fname)
            tmdb_data = get_movie(movie_data["Title"], movie_data["Released"])
            outfname = args.OUTPUT.rstrip('/') + '/' + fname if args.OUTPUT != '-' else '-'
            outfname = outfname.replace(':', " -")

            fmtdata = format_movie(movie_data, tmdb_data)
            write_movie(outfname, fmtdata)
            if outfname != '-':
                print(f"Writing to file {outfname}")

    if args.manual:
        year = args.year if args.manual else ''
        movie_data = {"Rewatch": 'no', "previous-watches": '',
                      "Title": args.manual, "LastSeen": '',
                      "notes": '', "Released": year,
                      "collection": args.collection if args.collection else ''}
        tmdb_data = get_movie(movie_data["Title"], movie_data["Released"])

        fmtdata = format_movie(movie_data, tmdb_data)

        fname = f"{tmdb_data.title} ({tmdb_data.year}).md"
        outfname = args.OUTPUT.rstrip('/') + '/' + fname if args.OUTPUT != '-' else '-'
        outfname = outfname.replace(':', " -")
        write_movie(outfname, fmtdata)
        if outfname != '-':
            print(f"Writing to file {outfname}")
