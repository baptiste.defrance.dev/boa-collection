#!/usr/bin/env python3
"""
Reads csv from notion and creates a bunch of markdown files with tags
for obsidian dataview
"""

import sys
import csv
import string

BOOKFORMAT = """\
---
tags: book
Author: {}
Completed: {}
Status: {}
Type: {}
Category: {}
Title: {}
Link: {}
---
"""

def convert_filename(filename):
    """
    Limit filename disgressions
    """
    valid_chars = f"-_.() {string.ascii_letters}{string.digits}"
    return ''.join(c for c in filename if c in valid_chars)

if __name__ == "__main__":
    csv_fname = sys.argv[1]
    books = []
    output_folder = sys.argv[2]

    with open(csv_fname, 'r', encoding="utf-8") as csv_file:
        csv_read=csv.DictReader(csv_file, delimiter=',')
        for el in csv_read:
            print(el)
            books.append(el)

    for book in books:
        fname = "{}/{}.md".format(output_folder, convert_filename(book["Name"].strip()))
        with open(fname, 'wt', encoding="utf-8") as outfile:
            outfile.write(BOOKFORMAT.format(book["Author"],
                                book["Completed"],
                                book["Status"],
                                book["Type"],
                                book["Category"],
                                book["Name"],
                                book["Link"]))
        print("Wrote {} to file {}.".format(book["Name"], fname))
