# Python scripts that don't really deserve online standalone repositories

### bookmanager.py - Reads csv from Notion.so exports and creates a bunch of markdown files with tags
hardcoded for books      

### mdtolist.py - Reads all the files in a folder and generates a txt list from the arguments given
For example all the markdown files in the folder "input/" have a tag named "Title" and are tagged 'movie':        
`mdtolist.py -f input/ -a Title` will display:
```
### movie List:
- [[path to file|name]] {Title}
- ...
- ...
```

### tmdbtool.py - Generate a markdown file containing all the info I need to sort out my movie watchlist
Requires 'themoviedb' module       
`tmdbtool.py -m "The Lord of the Rings" -y2003` will display all the info from the Lord of the Rings movie released in 2003 (return of the king):
```
---
tags: movie
LastSeen: 
Rewatch: no
previous-watches: 
Title: The Lord of the Rings - The Return of the King
Released: 2003
Categories: Adventure, Fantasy, Action
Director: Peter Jackson
Cast: Elijah Wood, Sean Astin, Viggo Mortensen, Andy Serkis, Ian McKellen, Billy Boyd
Duration: 3h21
language: en
imdb-link: https://www.imdb.com/title/tt0167260
added: 2023-09-28
collection: 
---
![poster](https://www.themoviedb.org/t/p/w185/rCzpDGLbOoPwLjy3OAm5NUPOTrC.jpg)
The eye of the enemy is moving.
#### Overview:
Aragorn is revealed as the heir to the ancient kings as he, Gandalf and the other members of the broken fellowship struggle to save Gondor from Sauron's forces. Meanwhile, Frodo and Sam take the ring closer to the heart of Mordor, the dark lord's realm.

#### Notes:

```

