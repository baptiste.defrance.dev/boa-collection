#!/usr/bin/env python3
"""
Generates a txt list from a markdown folder input
"""

import sys
from os import listdir, path
from os.path import isfile

TITLE_FORMAT = "### {} List:"
CONTENT_FORMAT = "- [[{}|{}]] {}"

def get_data(filename, attributes):
    """Extracts data from a markdown file"""
    data = {"tags": []}
    for attr in attributes:
        data[attr] = ''

    with open(filename, 'r', encoding="utf-8") as ofile:
        for lnumber, fline in enumerate(ofile):
            lcontent = fline.strip()
            if not lnumber or (lnumber < 2 and lcontent == "---"):
                continue

            if lcontent == "---":
                break

            if (lcontent.startswith("tags:") and
                len(lcontent.split("tags:")) == 2):
                data["tags"] += lcontent.split("tags: ")[1].split(", ")
            elif (lcontent.startswith("tag:") and
                len(lcontent.split("tag:")) == 2):
                data["tags"] += lcontent.split("tag: ")[1].split(", ")

            for attr in attributes:
                if (lcontent.startswith(f"{attr}:") and
                    len(lcontent.split(f"{attr}:")) == 2):
                    data[attr] = lcontent.split(f"{attr}:")[1]

    return data

def format_line(folder, filename, attributes):
    """Formats the line"""
    return CONTENT_FORMAT.format(folder + '/' + filename, filename, attributes)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Reads a folder input and \
            writes a txt list from .md files")
    parser.add_argument('-f', "--folder", type=str, help="Add a folder input")
    parser.add_argument('-a', "--attribute", type=str, action="append",
                        help="Adds an attribute as a display criteria")
    parser.add_argument("--format", type=str, help="Display format")
    args = parser.parse_args()

    if not args.folder:
        sys.stderr.write("No input specified.")
        sys.exit(1)

    tags = {}

    filelist = [f for f in listdir(args.folder) if isfile(path.join(args.folder, f))]
    for fname in filelist:
        if not fname.endswith(".md"):
            continue

        fdata = get_data(args.folder.rstrip('/') + '/' + fname, args.attribute)
        fname_noext = fname.split(".md")[0]

        for tag in fdata["tags"]:
            if tag not in tags:
                tags[tag] = []

            attrs = [fdata[a] for a in args.attribute]
            if len("".join(attrs).strip()) >= 2:
                fmt_line = format_line(args.folder.rstrip('/'), fname_noext, ",".join(attrs))
            else:
                fmt_line = format_line(args.folder.rstrip('/'), fname_noext, " ".join(attrs))
            tags[tag].append(fmt_line)

    for tag, lines in tags.items():
        print(TITLE_FORMAT.format(tag))
        for line in sorted(lines):
            print(line)
